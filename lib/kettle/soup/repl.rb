# frozen_string_literal: true

require_relative "repl/version"

module Kettle
  module Soup
    module Repl
      class Error < StandardError; end
      # Your code goes here...
    end
  end
end
